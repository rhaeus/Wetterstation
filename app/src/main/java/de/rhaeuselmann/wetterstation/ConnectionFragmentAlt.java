//package de.rhaeuselmann.wetterstation;
//
//
//import java.util.ArrayList;
//
//import android.app.Activity;
//import android.app.Fragment;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.CompoundButton;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.Switch;
//import android.widget.TextView;
//import android.widget.Toast;
//
//
//public class ConnectionFragmentAlt extends Fragment {
//
//	ConnectionReady mCallback;
//	//interface
//	 // Container Activity must implement this interface
//    public interface ConnectionReady {
//        public void onConnectionReady(BluetoothChatService mChatService);
//        public void onDataReceived(String data);
//    }
//
//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//
//        // This makes sure that the container activity has implemented
//        // the callback interface. If not, it throws an exception
//        try {
//            mCallback = (ConnectionReady) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement ConnectionReady");
//        }
//    }
//
//	////////////////////////////////////////
//
//	private static final int RESULT_OK = -1;
//	private static final int RESULT_CANCELED = 0;
//
//	private Switch BluetoothSwitch;
//	private TextView BluetoothTextView;
//	//private ListView BluetoothListView;
//
//	private ArrayList<String> list;
//	private ArrayAdapter<String> adapter = null;
//
//
//	public BluetoothAdapter mBluetoothAdapter = null;
//	private boolean discoveryRunning = false;
//
//	private Toast toast;
//
//	private Integer REQ_BT_ENABLE=1;
//	private Boolean registered = false;
//
//	private Menu myMenu;
//	private View myView;
//
//
//	// Message types sent from the BluetoothChatService Handler
//    public static final int MESSAGE_STATE_CHANGE = 1;
//    public static final int MESSAGE_READ = 2;
//    public static final int MESSAGE_WRITE = 3;
//    public static final int MESSAGE_DEVICE_NAME = 4;
//    public static final int MESSAGE_TOAST = 5;
//
//    // Key names received from the BluetoothChatService Handler
//    public static final String DEVICE_NAME = "device_name";
//    public static final String TOAST = "toast";
//
//    // Name of the connected device
//    private String mConnectedDeviceName = null;
//
//    // Member object for the chat services
//    private BluetoothChatService mChatService = null;
//
//    private Button testButton;
//    private Button connectionButton;
//
//
//	public ConnectionFragmentAlt(){}
//
////	public static ConnectionFragment newInstance() {
////		return new ConnectionFragment();
////	}
//
//
//
//	@Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//            Bundle savedInstanceState) {
//
//		//Layout laden
//        View rootView = inflater.inflate(R.layout.fragment_connection_neu, container, false);
//        myView = rootView;
//
//
//        //TEST
//        testButton = (Button) rootView.findViewById(R.id.testButton);
//        testButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                // Perform action on click
//            	sendMessage("0013");
//            	toast = Toast.makeText(getActivity().getApplicationContext(), "sendMessage 0013", Toast.LENGTH_SHORT);
//	    		  toast.show();
//            }
//        });
//
//
//
//
//
//
//
//
//
//        //BluetoothSwitch zuweisen
//        BluetoothSwitch = (Switch) rootView.findViewById(R.id.switchBluetooth);
//
//        //BluetoothTextview zuweisen
//        BluetoothTextView = (TextView) rootView.findViewById(R.id.TextViewBluetooth);
//
//        //BluetoothListView = (ListView) rootView.findViewById(R.id.listViewBTDevices);
//
//        connectionButton = (Button) rootView.findViewById(R.id.ConnectionButton);
//
//        mChatService = new BluetoothChatService(getActivity(), mHandler);
//
////    	BluetoothListView.setOnItemClickListener(new OnItemClickListener()
//// 	   {
//// 	      @Override
//// 	      public void onItemClick(AdapterView<?> adapter, View v, int position,
//// 	            long arg3)
//// 	      {
////
//// 	    	 if(mBluetoothAdapter.isDiscovering()){
//// 				mBluetoothAdapter.cancelDiscovery();
//// 			 }
////
//// 	    	// Get the device MAC address
//// 	    	  String PositionInfo = list.get(position);
//// 	    	  String[] split = PositionInfo.split("\n");
//// 	    	  String address = split[1];
////
//// 	    	//boolean check = mBluetoothAdapter.checkBluetoothAddress(address);
////
//// 	    	//toast = Toast.makeText(getActivity().getApplicationContext(), "-" + address + "-", Toast.LENGTH_LONG);
////           	//toast.show();
////
//// 	    	  if (address.equals("00:12:F3:23:36:6B")){
////
//// 	    		  	//Connect
//// 	                // Get the BLuetoothDevice object
//// 	                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
////
//// 	                // Attempt to connect to the device
//// 	               mChatService.connect(device);
////
//// 	           // Send the event to the host activity
//// 	              mCallback.onConnectionReady(mChatService);
////
//// 	    	  }
//// 	    	  else{
////
//// 	    		  toast = Toast.makeText(getActivity().getApplicationContext(), "Falsches Bluetooth-Geraet.\nEine Verbindung ist nur zur Wetterstation moeglich.", Toast.LENGTH_SHORT);
//// 	    		  toast.show();
//// 	    	  }
////
////
////
////
////
//// 	            //String value = (String)adapter.getItemAtPosition(position);
//// 	            // assuming string and if you want to get the value on click of list item
//// 	            // do what you intend to do on click of listview row
//// 	      }
//// 	   });
//
//
//
//        if((savedInstanceState != null) && (savedInstanceState.getSerializable("list") != null)){
//
//        	list = (ArrayList<String>) savedInstanceState.getSerializable("list");
//
//
//        	if (!list.isEmpty()){
//        		adapter = new ArrayAdapter<String> (getActivity(),android.R.layout.simple_list_item_1, list);
//        		myView.findViewById(R.id.textViewDevices).setVisibility(0); //0 -> visible
//            	BluetoothListView.setAdapter(adapter);
//        	}
//
//
//        }
//        else{
//        	list = new ArrayList();
//        }
//
//
//     // Register for broadcasts on BluetoothAdapter state change
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
//        filter.addAction(BluetoothDevice.ACTION_FOUND);
//        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
//        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//
//        getActivity().registerReceiver(mReceiver, filter);
//        registered = true;
//
//
//        //Bluetooth Adapter einrichten
//        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//
//        //ueberpruefen, ob Smartphone Bluetooth unterstuetzt
//        if (mBluetoothAdapter == null) {
//            // Device does not support Bluetooth
//        	Toast.makeText(getActivity().getApplicationContext(), "Device doesn't support Bluetooth", Toast.LENGTH_LONG).show();
//        }
//
//        //BluetoothSwitch Status bei Start anpassen
//        if (mBluetoothAdapter.isEnabled()){
//        	BluetoothSwitch.setChecked(true);
//        	BluetoothTextView.setText(R.string.bluetooth_tip_enabled);
//        }
//        else {
//        	BluetoothSwitch.setChecked(false);
//        	BluetoothTextView.setText(R.string.bluetooth_tip_disabled);
//        }
//
//
//
//
//
//        //auf �nderungen des BluetoothSwitches durch den Benutzer reagieren
//        BluetoothSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//
//
//            	if (isChecked) {
//                    // The switch is enabled
//
//            		//action bar refresh button aktivieren
//            		myMenu.findItem(R.id.connection_refresh).setVisible(true);
//
//
//            		//wenn Bluetooth deaktiviert ist, Bluetooth aktivieren
//            		if (!mBluetoothAdapter.isEnabled()) {
//            		    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            		    startActivityForResult(enableBtIntent, REQ_BT_ENABLE);
//            		}
//
//
//                } else {
//                    // The switch is disabled
//
//                	if(mBluetoothAdapter.isDiscovering()){
//                		mBluetoothAdapter.cancelDiscovery();
//
//                	}
//
//
//                	//action bar refresh button verstecken
//                	myMenu.findItem(R.id.connection_refresh).setVisible(false);
//
//                	//wenn Bluetooth aktiviert ist, deaktivieren
//                	if (mBluetoothAdapter.isEnabled()) {
//                	    mBluetoothAdapter.disable();
//                	}
//
//                	//liste leeren
//                	list.clear();
//                	//BluetoothListView.setAdapter(null);
//                	myView.findViewById(R.id.textViewDevices).setVisibility(4); //4 -> invisible
//
//                }
//            }
//        });
//
//        return rootView;
//    }
//
//
//
//
//	/**
//     * Sends a message.
//     * @param message  A string of text to send.
//     */
//    private void sendMessage(String message) {
//        // Check that we're actually connected before trying anything
//        /*if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
//            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
//            return;
//        }*/
//
//        // Check that there's actually something to send
//        if (message.length() > 0) {
//            // Get the message bytes and tell the BluetoothChatService to write
//            byte[] send = message.getBytes();
//            mChatService.write(send);
//
//            // Reset out string buffer to zero and clear the edit text field
//            //mOutStringBuffer.setLength(0);
//            //mOutEditText.setText(mOutStringBuffer);
//        }
//    }
//
//
//
//	/**
//	 * �brpr�fen, ob der Benutzer im Bluetooth aktivieren VDialog Ok oder Abbrechen gew�hlt hat
//	 */
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//		if(requestCode == REQ_BT_ENABLE){
//			if (resultCode == RESULT_OK){
//					Toast.makeText(getActivity().getApplicationContext(), "Bluetooth aktiviert", Toast.LENGTH_LONG).show();
//					BluetoothTextView.setText(R.string.bluetooth_tip_enabled);
//			}
//
//			if(resultCode == RESULT_CANCELED){
//				BluetoothSwitch.setChecked(false);
//				Toast.makeText(getActivity().getApplicationContext(), "Bluetooth nicht aktiviert!", Toast.LENGTH_LONG).show();
//
//			}
//		 }
//	 }//onActivityResult
//
//
//
//
//	/**
//	 * reagiert auf �nderungen des Bluetoothstatus
//	 * wenn bluetooth ausgeschalten wird (z.B �ber Einstellungen des Smartphones, nicht �ber diese App), wird bluetoothswitch=off
//	 * wenn bluetooth eingeschalten wird, wird bluetoothswitch= on
//	 */
//	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
//	    @Override
//	    public void onReceive(Context context, Intent intent) {
//	        final String action = intent.getAction();
//
//
//	        //BluetoothStaus ver�ndert
//	        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
//	            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
//	                                                 BluetoothAdapter.ERROR);
//
//	            switch (state) {
//	            case BluetoothAdapter.STATE_OFF:
//	            	BluetoothSwitch.setChecked(false);
//	            	BluetoothTextView.setText(R.string.bluetooth_tip_disabled);
//	                break;
//
//	            case BluetoothAdapter.STATE_TURNING_OFF:
//	            	toast = Toast.makeText(getActivity().getApplicationContext(), "Bluetooth wird deaktiviert", Toast.LENGTH_SHORT);
//                	toast.show();
//	                break;
//
//	            case BluetoothAdapter.STATE_ON:
//	            	BluetoothSwitch.setChecked(true);
//	            	BluetoothTextView.setText(R.string.bluetooth_tip_enabled);
//	                break;
//
//	            case BluetoothAdapter.STATE_TURNING_ON:
//	            	//toast = Toast.makeText(getActivity().getApplicationContext(), "Bluetooth wird aktiviert", Toast.LENGTH_SHORT);
//                	//toast.show();
//	                break;
//	            }
//	        }
//
//
//	        //Scan nach Ger�ten begonnen
//	        if(action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED)){
//	        	//discovery starts -> rotating refresh symbol in action bar
//	        	discoveryRunning = true;
//	        }
//
//	        //Scan nach Ger�ten beendet
//	        if(action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)){
//	        	//discovery finishes, dismis process dialog
//	        	discoveryRunning = false;
//	        	// Get our refresh item from the menu
//	            MenuItem m = myMenu.findItem(R.id.connection_refresh);
//
//                // Remove the animation.
//	            if(m.getActionView() != null){
//               m.getActionView().clearAnimation();
//               m.setActionView(null);
//	            }
//
//	            //toast = Toast.makeText(getActivity().getApplicationContext(), "Scan abgeschlossen", Toast.LENGTH_SHORT);
//            	//toast.show();
//
//
//	        }
//
//
//	        //Ger�t gefunden
//	        if(action.equals(BluetoothDevice.ACTION_FOUND)){
//	        	//bluetooth device found
//	        	BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//
//	        	//toast = Toast.makeText(getActivity().getApplicationContext(), "Found device " + device.getName(), Toast.LENGTH_SHORT);
//            	//toast.show();
//
//            	// Add the name and address to a list
//            	list.add(device.getName()+ "\n" + device.getAddress()+"\n");
//	        }
//
//	    }
//	};
//
//
//
//	private void stopDiscovery(){
//
//		if(mBluetoothAdapter.isDiscovering()){
//			mBluetoothAdapter.cancelDiscovery();
//
//
//
//
//		}
//
//	}
//
//
//
//
//
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setHasOptionsMenu(false);
//
//        this.setRetainInstance(true);
//
//	}
//
//
//
//	@Override
//	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//	    // Inflate the menu items for use in the action bar
//	    super.onCreateOptionsMenu(menu, inflater);
//
//	    //myMenu = menu;
//
////	    inflater.inflate(R.menu.connection_actions, menu);
//
////	    if(BluetoothSwitch.isChecked()){
////	    	myMenu.findItem(R.id.connection_refresh).setVisible(true);
////	    }
////	    else{
////	    	myMenu.findItem(R.id.connection_refresh).setVisible(false);
////	    }
//
//	}
//
//	@Override
////    public boolean onOptionsItemSelected(MenuItem item) {
////
////
////
////        // Handle action bar actions click
////        switch (item.getItemId()) {
////
////        case R.id.connection_refresh:
////
////        	if(!BluetoothSwitch.isChecked()){
////        		toast = Toast.makeText(getActivity().getApplicationContext(), "Bluetooth aktivieren!", Toast.LENGTH_SHORT);
////            	toast.show();
////
////        	}
////        	else{
////
////        		//scan
////        		list.clear();
////        		BluetoothListView.setAdapter(null);
////
////        		// Do animation start
////        		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////        		ImageView iv = (ImageView)inflater.inflate(R.layout.iv_refresh, null);
////        		Animation rotation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_refresh);
////        		rotation.setRepeatCount(Animation.INFINITE);
////        		iv.startAnimation(rotation);
////        		item.setActionView(iv);
////
////        		myView.findViewById(R.id.textViewDevices).setVisibility(0); //0 -> visible
////
////        		mBluetoothAdapter.startDiscovery();
////
////        	}
////
////            return true;
////
////        default:
////        	return super.onOptionsItemSelected(item);
////        }
////
////
////    }
//
//
//	 @Override
//	 public void onSaveInstanceState(Bundle outState) {
//	     super.onSaveInstanceState(outState);
//
//	     outState.putSerializable("list", list);
//
//
//	    }
//
//
//	    // The Handler that gets information back from the BluetoothChatService
//	    private final Handler mHandler = new Handler() {
//	        @Override
//	        public void handleMessage(Message msg) {
//	            switch (msg.what) {
//
//	            /*case MESSAGE_STATE_CHANGE:
//	                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
//	                switch (msg.arg1) {
//	                case BluetoothChatService.STATE_CONNECTED:
//	                    mTitle.setText(R.string.title_connected_to);
//	                    mTitle.append(mConnectedDeviceName);
//	                    mConversationArrayAdapter.clear();
//	                    break;
//	                case BluetoothChatService.STATE_CONNECTING:
//	                    mTitle.setText(R.string.title_connecting);
//	                    break;
//	                case BluetoothChatService.STATE_LISTEN:
//	                case BluetoothChatService.STATE_NONE:
//	                    mTitle.setText(R.string.title_not_connected);
//	                    break;
//	                }
//	                break;*/
//
//	            /*
//	            case MESSAGE_WRITE:
//	                byte[] writeBuf = (byte[]) msg.obj;
//	                // construct a string from the buffer
//	                String writeMessage = new String(writeBuf);
//	                mConversationArrayAdapter.add("Me:  " + writeMessage);
//	                break;*/
//
//	            case MESSAGE_READ:
//	                byte[] readBuf = (byte[]) msg.obj;
//	                // construct a string from the valid bytes in the buffer
//	                String m ="";
//
//	                for (int i = 0; i < 2; i++)
//	                {
//	                	m = m + String.format("%02X ", readBuf[i]);
//	                }
//
//	               // m = Byte.toString(readBuf[0]);
//
//	                //Integer.toHexString(-1);
//	                mCallback.onDataReceived(m);
//	                //String readMessage = new String(readBuf,0, msg.arg1);
//	                //mConversationArrayAdapter.add(mConnectedDeviceName+":  " + readMessage);
//	                //Toast.makeText(getActivity().getApplicationContext(), "Empfangen: "
//                            //+ m, Toast.LENGTH_LONG).show();
//
//
//	                break;
//
//	            case MESSAGE_DEVICE_NAME:
//	                // save the connected device's name
//	                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
//	                Toast.makeText(getActivity().getApplicationContext(), "Connected to "
//	                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
//	                break;
//	            case MESSAGE_TOAST:
//	                Toast.makeText(getActivity().getApplicationContext(), msg.getData().getString(TOAST),
//	                               Toast.LENGTH_SHORT).show();
//	                break;
//	            }
//	        }
//	    };
//
//
//
//
//	@Override
//	public void onPause() {
//	    super.onPause();
//
//	 // Unregister broadcast listeners
//	    getActivity().unregisterReceiver(mReceiver);
//	    registered = false;
//	}
//
//	 @Override
//	 public void onResume(){
//		 super .onResume();
//
//		 if(!registered){
//			 // Register for broadcasts on BluetoothAdapter state change
//	        IntentFilter filter = new IntentFilter();
//	        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
//	        filter.addAction(BluetoothDevice.ACTION_FOUND);
//	        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
//	        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//			getActivity().registerReceiver(mReceiver, filter);
//	     	registered = true;
//		 }
//
//	 }
//
//
//
//	@Override
//	public void onDestroy() {
//	    super.onDestroy();
//
//	    if(discoveryRunning){
//    		mBluetoothAdapter.cancelDiscovery();
//
//    	}
//
//
//	    // Unregister broadcast listeners
//	    if(registered){
//	    	getActivity().unregisterReceiver(mReceiver);
//	    }
//	}
//
//
//
//}
