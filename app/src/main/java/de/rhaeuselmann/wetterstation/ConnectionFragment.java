package de.rhaeuselmann.wetterstation;


import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;


public class ConnectionFragment extends Fragment {

    private static final String ADDRESS_WETTERSTATION = "00:12:F3:23:36:6B";


    ConnectionReady mCallback;

    //interface
    // Container Activity must implement this interface
    public interface ConnectionReady {
        public void onConnectionReady(BluetoothCommunication mChatService);

        public void onDataReceived(byte[] data);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (ConnectionReady) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ConnectionReady");
        }


    }

    ////////////////////////////////////////

    private Button testButton;

    //UI
    private Switch BluetoothSwitch;
    private TextView BluetoothTextView;
    private Button ConnectionButton;
    private Button DisconnectButton;
    private TextView ConnectionStatus;

    private ProgressBar mProgress;


    private View myView;
    private Toast toast;

    private BluetoothAdapter mBluetoothAdapter = null;
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Member object for the chat services
    private BluetoothCommunication mChatService = null;
    private boolean registered = false;

    private boolean WetterstationFound = false;

    //BT aktivieren
    private Integer REQ_BT_ENABLE = 1;

    //Bluetooth aktivieren Dialog
    private static final int RESULT_OK = -1;
    private static final int RESULT_CANCELED = 0;


    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONN_FAILED = 6;
    public static final int MESSAGE_CONN_LOST = 7;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";


    public ConnectionFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Layout laden
        View rootView = inflater.inflate(R.layout.fragment_connection_neu, container, false);
        myView = rootView;

        //Bluetooth Adapter einrichten
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //ueberpruefen, ob Smartphone Bluetooth unterstuetzt
        if (mBluetoothAdapter == null) {
            Toast.makeText(getActivity().getApplicationContext(), "Device doesn't support Bluetooth", Toast.LENGTH_LONG).show(); // Device does not support Bluetooth
            //getActivity().finish();
        }

        //UI beim Start anpassen
        initUI();

        handleBTSwitchChanges();

        BTChangesBroadcast();

        return rootView;
    }



    /**
     * je nach Bluetooth-Status (An/Aus) werden die UI-Elemente initialisiert
     * (sichtbar, nicht sichtbar, checjed, unchecked etc)
     */
   private void initUI()
   {

       //TEST
       testButton = (Button) myView.findViewById(R.id.testButton);
       testButton.setVisibility(View.GONE);
       //UI-Elemente zuweisen
       BluetoothSwitch = (Switch) myView.findViewById(R.id.switchBluetooth);
       BluetoothTextView = (TextView) myView.findViewById(R.id.TextViewBluetooth);
       ConnectionStatus = (TextView) myView.findViewById(R.id.ConnectionStatus);
       ConnectionButton = (Button) myView.findViewById(R.id.ConnectionButton);
       DisconnectButton = (Button) myView.findViewById(R.id.DisconnectButton);
       mProgress = (ProgressBar) myView.findViewById(R.id.progressBar);
       mProgress.setVisibility(View.INVISIBLE);


       testButton.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v)
           {
               // Perform action on click
               //sendMessage("0013");
               byte[] send = new byte[4];



               //Sensor 1
               send[0] = 0x00;
               //send[1] = 0x00;
               //send[2] = 0x01;
               //send[3] = 0x03;

               mChatService.write(send);
               toast = Toast.makeText(getActivity().getApplicationContext(), "sendMessage 00", Toast.LENGTH_SHORT);
               toast.show();
           }
       });

       ConnectionButton.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
               WetterstationFound = false;
               showProgressBar(true);

               if(alreadyPaired())
               {
                   connectToWetterstation();
               }
               else
               {
                   mBluetoothAdapter.startDiscovery();
               }
           }
       });


       DisconnectButton.setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
               //cancel connection
               mChatService.stop();
           }
       });


       updateUI();




       // Initialize the BluetoothCommunication to perform bluetooth connections
       mChatService = new BluetoothCommunication(getActivity(), mHandler);
   }

    private void updateUI()
    {
        if (mBluetoothAdapter.isEnabled()){
            BluetoothSwitch.setChecked(true);
            UI_BT_On();
        }
        else {
            BluetoothSwitch.setChecked(false);
            UI_BT_Off();
        }
    }

    /**
     * checks if Wetterstation is in the list of paired devices
     * @return true if already paired, false if not
     */
    private boolean alreadyPaired()
    {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        //If there are paired devices
        if(pairedDevices.size() > 0)
        {
            for( BluetoothDevice device : pairedDevices )
            {
                if( device.getAddress().equals(ADDRESS_WETTERSTATION) )
                {
                    return true;
                }
            }
        }
        return false;
    };

    /**
     * reagiert auf Änderungen des BluetoothSwitches durch den Benutzer
     * schaltet BT an oder aus
     * und passt den Anzeigestatus der UI-Elemente an (visible, invisible)
     */
    private void handleBTSwitchChanges() {
        BluetoothSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                { // The switch is enabled
                    if (!mBluetoothAdapter.isEnabled())
                    {
                        turnOnBT();
                    }
                } else
                {// The switch is disabled
                    if(mBluetoothAdapter.isDiscovering())
                    {
                        mBluetoothAdapter.cancelDiscovery();
                    }
                    if (mBluetoothAdapter.isEnabled())
                    {
                        turnOffBT();
                    }
                }
            }
        });
    }

    //------eigene Hilfsfunktionen

    private void turnOnBT(){
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQ_BT_ENABLE);
    }

    private void turnOffBT(){
        mBluetoothAdapter.disable();
    }

    private void UI_BT_Off(){
        ConnectionButton.setVisibility(View.INVISIBLE);
        ConnectionStatus.setVisibility(View.INVISIBLE);
        DisconnectButton.setVisibility(View.INVISIBLE);
        ConnectionStatus.setVisibility(View.INVISIBLE);
        BluetoothTextView.setText(R.string.bluetooth_tip_disabled);
        BluetoothSwitch.setChecked(false);
    }

    private void UI_BT_On(){
        ConnectionButton.setVisibility(View.VISIBLE);
        ConnectionStatus.setVisibility(View.VISIBLE);
        DisconnectButton.setVisibility(View.VISIBLE);
        ConnectionStatus.setVisibility(View.VISIBLE);
        BluetoothTextView.setText(R.string.bluetooth_tip_enabled);
        BluetoothSwitch.setChecked(true);
    }

    /**
     * Register for broadcasts on BluetoothAdapter state change
     */
    private void BTChangesBroadcast(){

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        getActivity().registerReceiver(mReceiver, filter);
        registered = true;
    }

    private void connectToWetterstation()
    {
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(ADDRESS_WETTERSTATION);

        // Attempt to connect to the device
 	    mChatService.connect(device);

 	    // Send the event to the host activity
 	    //mCallback.onConnectionReady(mChatService);
    }


    //------------------------------------
    //--------Override Methoden-----------
    //------------------------------------
    /**
     * Übrprüfen, ob der Benutzer im Bluetooth aktivieren Dialog Ok oder Abbrechen gewählt hat
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQ_BT_ENABLE){
            if (resultCode == RESULT_OK){
                //Toast.makeText(getActivity().getApplicationContext(), "Bluetooth aktiviert", Toast.LENGTH_LONG).show();
                //BluetoothTextView.setText(R.string.bluetooth_tip_enabled);
                UI_BT_On();
            }

            if(resultCode == RESULT_CANCELED){
                BluetoothSwitch.setChecked(false);
                //Toast.makeText(getActivity().getApplicationContext(), "Bluetooth nicht aktiviert!", Toast.LENGTH_LONG).show();
                UI_BT_Off();

            }
        }
    }//onActivityResult


    private void setConnectionStatus(int id)
    {
        ConnectionStatus.setText(id);
    }

    private void setConnectionStatus(String text)
    {
        ConnectionStatus.setText(text);
    }

    /**
     * reagiert auf Änderungen des Bluetoothstatus
     * wenn bluetooth ausgeschalten wird (z.B �ber Einstellungen des Smartphones, nicht �ber diese App), wird bluetoothswitch=off
     * wenn bluetooth eingeschalten wird, wird bluetoothswitch= on
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            //BluetoothStatus verändert
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);

                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        UI_BT_Off();
                        break;

                    case BluetoothAdapter.STATE_TURNING_OFF:
                        //toast = Toast.makeText(getActivity().getApplicationContext(), "Bluetooth wird deaktiviert", Toast.LENGTH_SHORT);
                        //toast.show();
                        break;

                    case BluetoothAdapter.STATE_ON:
                        UI_BT_On();
                        break;

                    case BluetoothAdapter.STATE_TURNING_ON:
                        //toast = Toast.makeText(getActivity().getApplicationContext(), "Bluetooth wird aktiviert", Toast.LENGTH_SHORT);
                        //toast.show();
                        break;
                }
            }


            //Scan nach Geräten begonnen
            if(action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED)){
                //mProgress.setVisibility(View.VISIBLE);
                setConnectionStatus(R.string.connStatusSearching);
            }

            //Scan nach Geräten beendet
            if(action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)){
                //discovery finishes, dismis process dialog
                showProgressBar(false);

                if( WetterstationFound )
                {
                    setConnectionStatus(R.string.connStatusWetterstationFound);
                }
                else
                {
                    setConnectionStatus(R.string.connStatusWetterstationNotFound);
                }
            }


            //Gerät gefunden
            if(action.equals(BluetoothDevice.ACTION_FOUND)){
                //bluetooth device found
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                //toast = Toast.makeText(getActivity().getApplicationContext(), "Found device " + device.getName(), Toast.LENGTH_SHORT);
                //toast.show();

                //wenn Wetterstation gefunden -> flag auf true, discovery abbrechen
                if (device.getAddress().equals(ADDRESS_WETTERSTATION)){
                    WetterstationFound = true;
                    mBluetoothAdapter.cancelDiscovery();
                    connectToWetterstation();
                }

            }

        }
    };

    /**
     * progressbar (animierter kreis) anzeigen oder verbergen
     * @param show true anzeigen, false verbergen
     */
    private void showProgressBar(boolean show)
    {
        if(show){
            mProgress.setVisibility(View.VISIBLE);
        }
        else
        {
            mProgress.setVisibility(View.INVISIBLE);
        }
    }


    /**
     * The Handler that gets information back from the BluetoothCommunication
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothCommunication.STATE_CONNECTED:
                            mCallback.onConnectionReady(mChatService);
                            showProgressBar(false);
                            setConnectionStatus(R.string.connStatusWetterstationConnected);
                            break;
                        case BluetoothCommunication.STATE_CONNECTING:
                            setConnectionStatus(R.string.connStatusWetterstationConnecting);
                            break;
                        case BluetoothCommunication.STATE_NONE:
                            mCallback.onConnectionReady(null);
                            setConnectionStatus(R.string.connStatusWetterstationNotConnected);
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    byte[] answer = new byte[3];

	                for (int i = 0; i < 3; i++)
	                {
                        answer[i] = readBuf[i];
	                }
                    mCallback.onDataReceived(answer);
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    break;
                case MESSAGE_CONN_FAILED:
                    mCallback.onConnectionReady(null);
                    setConnectionStatus(msg.getData().getString(TOAST));
                    break;
                case MESSAGE_CONN_LOST:
                    mCallback.onConnectionReady(null);
                    setConnectionStatus(msg.getData().getString(TOAST));
                    break;
                case MESSAGE_TOAST:
                    if (null != getActivity()) {
                        Toast.makeText(getActivity(), msg.getData().getString(TOAST),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };






    @Override
    public void onPause() {
        super.onPause();

        // Unregister broadcast listeners
        getActivity().unregisterReceiver(mReceiver);
        registered = false;
    }

    @Override
    public void onResume(){
        super .onResume();

        if(!registered){
            // Register for broadcasts on BluetoothAdapter state change
            BTChangesBroadcast();
        }
        updateUI();

    }



    @Override
    public void onDestroy() {
        super.onDestroy();

        if(mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
        }

        //Stop all threads
        if (mChatService != null) {
            mChatService.stop();
        }

        // Unregister broadcast listeners
        if(registered){
            getActivity().unregisterReceiver(mReceiver);
        }
    }
}