
package de.rhaeuselmann.wetterstation;

import android.os.Bundle;
import android.preference.PreferenceFragment;



public  class SelectSensorsPreferenceFragment extends PreferenceFragment {
	
	
//	public static SelectSensorsPreferenceFragment newInstance() {
//		return new SelectSensorsPreferenceFragment();
//	}
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.preferences);
    }
}