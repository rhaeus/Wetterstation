//package de.rhaeuselmann.wetterstation;
//
//import java.util.ArrayList;
//import java.util.Locale;
//
//import android.app.Fragment;
//import android.app.FragmentManager;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.preference.PreferenceManager;
//import android.support.v13.app.FragmentPagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.text.format.Time;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.widget.ArrayAdapter;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//
//
//import com.viewpagerindicator.TitlePageIndicator;
//import com.viewpagerindicator.TitlePageIndicator.IndicatorStyle;
////import android.support.v13.app.FragmentPagerAdapter;
//
//
//public class MeasureFragment extends Fragment {
//
//	public static final int MESSAGE_READ = 2;
//
//	 // Member object for the chat services
//    public BluetoothCommunication mChatService = null;
//
//	private MyListAdapter adapter = null;
//	private ListView listView;
//	private TextView refreshTime;
//
//	private Menu myMenu;
//	private View curMeasView;
//
//	private Toast toast;
//
//	public int selectedPage = -1;
//
//	private ArrayList<MeasVal> vals =  new ArrayList<MeasVal>(); // Construct the data source
//
//	//startView Pager
//	SectionsPagerAdapter mSectionsPagerAdapter;
//
//	public static final String TAG = MeasureFragment.class.getSimpleName();
//
//	TitlePageIndicator indicator = null;
//	//endViewpager
//
//
//	/**
//	 * The {@link ViewPager} that will host the section contents.
//	 */
//	ViewPager mViewPager;
//	//////
//
//
//
//	public MeasureFragment(){
//
//	}
//
//	public static MeasureFragment newInstance() {
//		return new MeasureFragment();
//	}
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setHasOptionsMenu(true);
//
//		this.setRetainInstance(true);
//	}
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		//View rootView = inflater.inflate(R.layout.ac_meas_layout,container, false);
//
//
//		//viewpager start
//		View rootView = inflater.inflate(R.layout.viewpager_meas, container, false);
//		mSectionsPagerAdapter = new SectionsPagerAdapter(
//				getFragmentManager(), getActivity());
//
//		mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
//		mViewPager.setAdapter(mSectionsPagerAdapter);
//
//
//		//Bind the title indicator to the adapter
//		 indicator = (TitlePageIndicator)rootView.findViewById(R.id.indicator);
//		 indicator.setViewPager(mViewPager);
//
//
//		 //Styling Tabs
//
//	        final float density = getResources().getDisplayMetrics().density;
//	        indicator.setBackgroundColor(Color.parseColor("#1A237E"));
//	        //indicator.setFooterColor(0xFFAA2222);
//	        indicator.setFooterColor(0xFFFFFFFF);
//	        indicator.setFooterLineHeight(3 * density); //1dp
//	        indicator.setFooterIndicatorHeight(5 * density); //3dp
//	        indicator.setFooterIndicatorStyle(IndicatorStyle.Underline);
//	        indicator.setTextColor(0xFFFFFFFF);
//	        indicator.setSelectedColor(0xFFFFFFFF);
//	        indicator.setSelectedBold(true);
//	     //viewpager end
//
//
//		return rootView;
//	}
//
//
//
//
//	@Override
//	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//	    // Inflate the menu items for use in the action bar
//	    super.onCreateOptionsMenu(menu, inflater);
//	    myMenu = menu;
//        inflater.inflate(R.menu.measure_actions, menu);
//
//
//
//	}
//
//
//	@Override
//    public void onPrepareOptionsMenu(Menu menu) {
//		super.onPrepareOptionsMenu(menu);
//		/*
//		if(selectedPage == 1){
//        	menu.findItem(R.id.action_add_sensors).setVisible(false);
//        	menu.findItem(R.id.action_refresh).setVisible(false);
//
//        }*/
//
//    }
//
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//	    // Handle presses on the action bar items
//	    switch (item.getItemId()) {
//	        case R.id.action_refresh:
//
//	        	// Do animation start
//        		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        		ImageView iv = (ImageView)inflater.inflate(R.layout.iv_refresh, null);
//        		Animation rotation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_refresh);
//        		rotation.setRepeatCount(Animation.INFINITE);
//        		iv.startAnimation(rotation);
//        		item.setActionView(iv);
//
//
//	        	Toast.makeText(getActivity().getApplicationContext(), "Werte einlesen", Toast.LENGTH_SHORT).show();
//
//	        	MeasVal measValues = new MeasVal();
//
//	        	ArrayList<MeasVal> values = measValues.getValues();
//
//	        	adapter = new MyListAdapter(getActivity(), values);
//	        	listView.setAdapter(adapter);
//
//
//	        	// Remove the animation.
//	        	// Get our refresh item from the menu
//	            //MenuItem m = myMenu.findItem(R.id.connection_refresh);
//                item.getActionView().clearAnimation();
//                item.setActionView(null);
//
//	            return true;
//
//	        case R.id.action_add_sensors:
//	        	//preferences oeffnen -> SelectSensorsActivity starten
//	        	Intent intent = new Intent(getActivity(), SelectSensorsActivity.class);
//	        	startActivity(intent);
//
//	            return true;
//	        default:
//	            return super.onOptionsItemSelected(item);
//	    }
//	}
//
//
//	public void addReceivedData(String rData){
//
//		vals.add(new MeasVal(getResources().getString(R.string.sensor1_title), rData,"°C"));
//		adapter = new MyListAdapter(getActivity(), vals);
//    	listView.setAdapter(adapter);
//	}
//
//
//
//	//////////////////////////
//
//
//	public class MeasVal {
//	    public String name;
//	    public String value;
//	    public String unit;
//
//	    //SensorSettings
//	    private Boolean Sensor1;
//	    private Boolean Sensor2;
//	    private Boolean Sensor3;
//	    private Boolean Sensor4;
//
//	    private String time = null;
//
//	    private boolean rec = false;
//
//	    //Verarbeitung bluetooth Befehle
//	    private int[] commands = {-1, -1, -1, -1}; //enthaelt Reihenfolge der Befehle
//	    private int commandCounter = 0; //zaehlt abgearbeitete Befehle
//
//	    //public ArrayList<MeasVal> vals =  new ArrayList<MeasVal>(); // Construct the data source
//
//	    private BluetoothAdapter mBluetoothAdapter = null;
//
//	    public MeasVal(){}
//
//	    public MeasVal(String name, String value, String unit) {
//	       this.name = name;
//	       this.value = value;
//	       this.unit = unit;
//	    }
//
//
//
//
//
//	    /**
//	     * dummyfunktion um Auslesen der Daten von der Station zu simulieren
//	     */
//	    public ArrayList<MeasVal> getValues(){
//
//	    	byte[] send = new byte[4];
//
//	    	writeRefreshTime();
//
//	    	getSettings();
//
//	    	vals.clear();
//
//	    	//Sensor 1
//	    	send[0] = 0x00;
//	        send[1] = 0x00;
//	        send[2] = 0x01;
//	        send[3] = 0x03;
//
//	    	mChatService.write(send);
//
//	    		  	//Connect
//	                // Get the BLuetoothDevice object
//	         //BluetoothDevice device = mBluetoothAdapter.getRemoteDevice("00:12:F3:23:36:6B");
//
//	                // Attempt to connect to the device
//	              // mChatService.connect(device);
//
//
//
//	    	//chooseCommands();
//
//	    	// Construct the data source
//			//ArrayList<MeasVal> vals =  new ArrayList<MeasVal>();
//
//
//			//doCommand(getCommand());
//
//			/*
//			 * Dummywerte
//
//
//			if (Sensor1){//Temperatur
//				//getTemperature();
//				vals.add(new MeasVal(getResources().getString(R.string.sensor1_title), "20","°C"));
//			}
//
//			if (Sensor2){
//				vals.add(new MeasVal("Luftfeuchtigkeit", "50","%"));
//			}
//
//			if (Sensor3){
//				vals.add(new MeasVal("Lichtstaerke", "500","Lux"));
//			}
//
//			if (Sensor4){
//				vals.add(new MeasVal("Niederschlag", "","Ja"));
//			}
//
//			*/
//
//
//	    	return vals;
//	    }
//
//
//
//
//
//
//	    private void getSettings(){
//
//	    	SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//
//	    	Sensor1 = sharedPref.getBoolean("sensor1", false);
//	    	Sensor2 = sharedPref.getBoolean("sensor2", false);
//	    	Sensor3 = sharedPref.getBoolean("sensor3", false);
//	    	Sensor4 = sharedPref.getBoolean("sensor4", false);
//
//
//
//	    }
//
//	    private void writeRefreshTime(){
//
//	    	Time now = new Time();
//	    	now.setToNow();
//
//
//	    	time = now.format("%H:%M:%S Uhr  %d.%m.%Y");
//
//	    	curMeasView.findViewById(R.id.textViewLastRefresh).setVisibility(View.VISIBLE); //0 -> visible
//
//	    	refreshTime.setText("zuletzt aktualisiert: " + time);
//
//
//	    }
//
//	}
//
//
//
//
//	public class MyListAdapter extends ArrayAdapter<MeasVal> {
//	    public MyListAdapter(Context context, ArrayList<MeasVal> measval) {
//	       super(context, 0, measval);
//	    }
//
//	    @Override
//	    public View getView(int position, View convertView, ViewGroup parent) {
//	       // Get the data item for this position
//	       MeasVal measval = getItem(position);
//	       // Check if an existing view is being reused, otherwise inflate the view
//	       if (convertView == null) {
//	          convertView = LayoutInflater.from(getContext()).inflate(R.layout.ac_meas_list_item, parent, false);
//	       }
//	       // Lookup view for data population
//	       TextView tvName = (TextView) convertView.findViewById(R.id.name);
//	       TextView tvValue = (TextView) convertView.findViewById(R.id.value);
//	       TextView tvUnit = (TextView) convertView.findViewById(R.id.unit);
//	       // Populate the data into the template view using the data object
//	       tvName.setText(measval.name);
//	       tvValue.setText(measval.value);
//	       tvUnit.setText(measval.unit);
//	       // Return the completed view to render on screen
//	       return convertView;
//	   }
//	}
//
//
//
//
//
//
//
///**
//* A {@link FragmentPagerAdapter} that returns a fragment corresponding to
//* one of the sections/tabs/pages.
//*/
//public class SectionsPagerAdapter extends FragmentPagerAdapter {
//
//	private Context mContext = null;
//
//
//	public SectionsPagerAdapter(FragmentManager fm, Context context) {
//		super(fm);
//		mContext = context;
//	}
//
//	@Override
//	public Fragment getItem(int position) {
//		// getItem is called to instantiate the fragment for the given page.
//
//		Fragment fragment = null;
//
//
//		switch (position){
//		case 0:
//			fragment = new CurrentMeasValuesFragment();
//			break;
//
//		case 1:
//			fragment = new HistoryMeasValuesFragment();
//			break;
//
//		}
//
//		return fragment;
//
//	}
//
//	@Override
//	public int getCount() {
//		// Show 2 total pages.
//		return 2;
//	}
//
//	@Override
//	public CharSequence getPageTitle(int position) {
//		Locale l = Locale.getDefault();
//
//
//
//		switch (position) {
//		case 0:
//
//			return mContext.getString(R.string.measure_title_page_1).toUpperCase(l);
//		case 1:
//
//			return mContext.getString(R.string.measure_title_page_2).toUpperCase(l);
//		}
//		return null;
//	}
//
//}
//
////////////////////////////
///**
//* aktuelle Messwerte Fragment
//*/
//public class CurrentMeasValuesFragment extends Fragment{
//
//	MyListAdapter adapter = null;
//
//	//Constructor
//	public CurrentMeasValuesFragment() {
//		}
//
//
//
//
//
//	@Override
//	public void onCreate(Bundle savedInstanceState){
//		super.onCreate(savedInstanceState);
//	}
//
//
//	//onCreate, Layout
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//		Bundle savedInstanceState) {
//
//		View view = inflater.inflate(R.layout.ac_meas_layout, null, false);
//		curMeasView = view;
//		listView = (ListView) view.findViewById(R.id.listView1);
//		refreshTime = (TextView) view.findViewById(R.id.textViewLastRefresh);
//
//		return view;
//	}
//
//
//}
//
//
///////////////////////////////////////
///**
//* Verlauf der Messwerte Fragment
//*/
//public  class HistoryMeasValuesFragment extends Fragment{
//
//	//Constructor
//	public HistoryMeasValuesFragment() {
//	}
//
//
//	//onCreate, Layout
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//		Bundle savedInstanceState) {
//
//		View rootView = inflater.inflate(R.layout.history_meas,container, false);
//
//		//View rootView = inflater.inflate(R.layout.fragment_tabbed_dummy,container, false);
//		//TextView dummyTextView = (TextView) rootView
//		//.findViewById(R.id.section_label);
//
//		//dummyTextView.setText("Verlauf der Messwerte");
//		return rootView;
//	}
//
//
//}
//
//
//}
//
//
//
//
//
//
//	///////////////////////////////////////////////////////////////////////////////
//
//	/**
//	 * A dummy fragment representing a section of the app, but that simply
//	 * displays dummy text.
//	 *//*
//	public static class DummySectionFragment extends Fragment {
//		/**
//		 * The fragment argument representing the section number for this
//		 * fragment.
//		 *//*
//		public static final String ARG_SECTION_NUMBER = "section_number";
//
//		public DummySectionFragment() {
//		}
//
//		@Override
//		public View onCreateView(LayoutInflater inflater, ViewGroup container,
//				Bundle savedInstanceState) {
//			View rootView = inflater.inflate(R.layout.fragment_tabbed_dummy,
//					container, false);
//			TextView dummyTextView = (TextView) rootView
//					.findViewById(R.id.section_label);
//			dummyTextView.setText(Integer.toString(getArguments().getInt(
//					ARG_SECTION_NUMBER)));
//			return rootView;
//		}
//	}
//
//}*/
