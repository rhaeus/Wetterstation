package de.rhaeuselmann.wetterstation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.widget.Toast;

import java.io.File;


public  class SettingsPreferenceFragment extends PreferenceFragment {
	
	
//	public static SelectSensorsPreferenceFragment newInstance() {
//		return new SelectSensorsPreferenceFragment();
//	}
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.settings_preferences);


        Preference myPref = (Preference) findPreference("clearLog");
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                openDialog();
                return true;
            }
        });
        
    }

    /**
     * Erstellt und öffnet einen Dialog -> Bestätigungsdialog zum Löschen der Logfiles
     * Der Dialog hat zwei Buttons:
     * Ok -> Logfiles werden gelöscht
     * Abbrechen -> Logfiles werden nicht gelöscht
     */
    private void openDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);

        // Add the buttons
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                deleteData();
                //Toast.makeText(getActivity().getApplicationContext(), "Daten gelöscht!", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });


        // Create the AlertDialog
        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void deleteData()
    {
        File file = new File(getActivity().getFilesDir(), "Temp");
        file.delete();

        file = new File(getActivity().getFilesDir(), "Humidity");
        file.delete();

        file = new File(getActivity().getFilesDir(), "Luminosity");
        file.delete();

        file = new File(getActivity().getFilesDir(), "Rain");
        file.delete();

        resetDatasetCounter();

    }

    private void resetDatasetCounter()
    {
        //get handle to shared preference
        Context context = getActivity();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.dataset_counter_preference_file_key), Context.MODE_PRIVATE);


        //write to shared preference
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.dataset_counter), 0);
        editor.commit();
    }




}