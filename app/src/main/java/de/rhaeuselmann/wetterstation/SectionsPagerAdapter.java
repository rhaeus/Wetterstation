package de.rhaeuselmann.wetterstation;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v13.app.FragmentCompat;
import android.support.v13.app.FragmentPagerAdapter;
import android.app.FragmentManager;
import android.util.Log;
import android.view.ViewGroup;

import java.util.Locale;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private Context mContext = null;




    public SectionsPagerAdapter(FragmentManager fm, Context context ) {
        super(fm);
        mContext = context;

    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.

        Fragment fragment = null;


        switch (position){
            case 0:
                fragment = new DisplayValuesFragment();
                break;

            case 1:
                fragment = new ChartFragment();
                break;

        }

        return fragment;

    }
//    @Override
public Object instantiateItem(ViewGroup container, int position) {
    final Fragment fragment = (Fragment) super.instantiateItem(container, position);
    //final TabInfo info = mTabs.get(position);
    //info.tag = fragment.getTag(); // set it here
    String tag = fragment.getTag();
    return fragment;
}

    private static String makeFragmentName(int position) {
        return "android:switcher:" + position;
    }



    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();



        switch (position) {
            case 0:

                return mContext.getString(R.string.measure_title_page_1).toUpperCase(l);
            case 1:

                return mContext.getString(R.string.measure_title_page_2).toUpperCase(l);
        }
        return null;
    }

}