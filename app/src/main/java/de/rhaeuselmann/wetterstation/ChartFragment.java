package de.rhaeuselmann.wetterstation;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidplot.Plot;
import com.androidplot.ui.AnchorPosition;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.ui.XLayoutStyle;
import com.androidplot.ui.YLayoutStyle;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
/**
 * Created by Ramona on 19.04.2015.
 * Verlauf der Messwerte Fragment
 */
public class ChartFragment extends Fragment {

    private XYPlot plot;
    private Button myButton;

    private View myView;
    private static final int DEFAULT_MAX_DATA = 200;
    private int MAX_DATA = 200;
    private int datasetcount = 0;

    private Long[] xValues;// = new Long[MAX_DATA];
    private Double[] yValues;// = new Double[MAX_DATA];
    private int count;

    private RadioGroup radioGroup;
    private EditText numberOfDataPoints;




    public ChartFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.simple_xy_plot_example, container, false);
        myView = rootView;
        initUI();

        return rootView;
    }

    private void displayData()
    {
        plot.clear();
        if(count == 0)
        {
            Toast.makeText(getActivity().getApplicationContext(), "Keine Daten vorhanden!", Toast.LENGTH_SHORT).show();
            return;
        }

        XYSeries mySeries = new SimpleXYSeries(
                Arrays.asList(xValues),
                Arrays.asList(yValues),
                "mySeries");

        // Create a formatter to use for drawing a series using LineAndPointRenderer
        // and configure it from xml:
        LineAndPointFormatter series1Format = new LineAndPointFormatter();
        series1Format.setPointLabelFormatter(new PointLabelFormatter());
        series1Format.configure(getActivity().getApplicationContext(),
                R.xml.line_point_formatter_with_plf1);

        // add a new series' to the xyplot:
        plot.addSeries(mySeries, series1Format);

        final String myDateformat;

        if( (xValues[count-1] - xValues[0]) <= 86400000 ) //86400000ms = 24h
        {
            myDateformat = "HH:mm";
        }
        else
        {
            myDateformat = "dd.MM HH:mm";
        }


        plot.setDomainValueFormat(new Format() {

            // create a simple date format that draws on the year portion of our timestamp.
            // see http://download.oracle.com/javase/1.4.2/docs/api/java/text/SimpleDateFormat.html
            // for a full description of SimpleDateFormat.
            private SimpleDateFormat dateFormat = new SimpleDateFormat(myDateformat);

            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {

                // because our timestamps are in seconds and SimpleDateFormat expects milliseconds
                // we multiply our timestamp by 1000:
                long timestamp = ((Number) obj).longValue() ;
                Date date = new Date(timestamp);
                return dateFormat.format(date, toAppendTo, pos);
            }

            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;

            }
        });

        adjustChartFormatting();

        plot.redraw();
    }

    private double getYMin()
    {
        double min = yValues[0];
        for(int i = 0; i < count; i++)
        {
            if(yValues[i] < min)
            {
                min = yValues[i];
            }
        }
        return min;
    }

    private double getYMax()
    {
        double max = yValues[0];
        for(int i = 0; i < count; i++)
        {
            if(yValues[i] > max)
            {
                max = yValues[i];
            }
        }
        return max;
    }

    private void adjustChartFormatting()
    {
        Sensors checkedSensor = getCheckedSensor();

        plot.setRangeValueFormat(new DecimalFormat("0.0"));

        //runden in 5er-schritten


        int min = (int)(getYMin()/5) * 5 ;
        int max = (int)(getYMax()/5) * 5 + 5;

//        if((max - min) <= 10)
//        {
//            min = (int)(getYMin()); //abrunden
//            max = (int)(getYMax()) + 1;//aufrunden
//        }


        //plot.setTicksPerRangeLabel(5);

        plot.setRangeBoundaries(min,max,BoundaryMode.FIXED);

        switch(checkedSensor){
            case NONE:
                return;
            case TEMPERATURE:
                plot.setRangeLabel("Temperatur (°C)");

                plot.setRangeLowerBoundary(min,BoundaryMode.FIXED);
                plot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 5);
                //plot.setRangeStep(XYStepMode.SUBDIVIDE, ((max-min)/10+1)*2); //5er schritte
                if((max - min) <= 10)
                {
//                    plot.setRangeStep(XYStepMode.SUBDIVIDE, (max-min)+1); //1er schritte
                    plot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 1);
                }
                return;
            case HUMIDITY:
                plot.setRangeLabel("Luftfeuchtigkeit (%)");
                plot.setRangeLowerBoundary(min,BoundaryMode.FIXED);
                plot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 5);
//                plot.setRangeStep(XYStepMode.SUBDIVIDE, ((max-min)/10+1)*2);//5er schritte
                if((max - min) <= 10)
                {
                    plot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 1);
                }
                return;
            case LUMINOSITY:
                plot.setRangeLabel("Umgebungshelligkeit (lux)");
                plot.setRangeLowerBoundary(min,BoundaryMode.FIXED);
                plot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 10);
//                plot.setRangeStep(XYStepMode.SUBDIVIDE, ((max-min)/10+1));//10er schritte
                if((max - min) <= 10)
                {
                    plot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 1);
                }
                return;
            case RAIN:
                plot.setRangeLabel("Regen (0 = Nein, 1 = Ja)");
                plot.setRangeBoundaries(0,1,BoundaryMode.FIXED);
                plot.setRangeValueFormat(new DecimalFormat("#"));
                plot.setRangeStep(XYStepMode.SUBDIVIDE, 2);
                //plot.setTicksPerRangeLabel(3);

                return;
            default:
                return;
        }
    }





    private void formatChart()
    {


        //colors
        //background

        plot.getGraphWidget().getBackgroundPaint().setColor(Color.TRANSPARENT); //getResources().getColor(R.color.fragmentbackgroundcolor)); //farbe um zahlen

        plot.getGraphWidget().getGridBackgroundPaint().setColor(Color.TRANSPARENT);//getResources().getColor(R.color.my_red)); //farbe wo grid ist
        //zahlen auf achse
        plot.getGraphWidget().getDomainLabelPaint().setColor(Color.BLACK);
        plot.getGraphWidget().getRangeLabelPaint().setColor(Color.BLACK);
        //achsenbesschriftung
        plot.getDomainLabelWidget().getLabelPaint().setColor(Color.BLACK);
        plot.getRangeLabelWidget().getLabelPaint().setColor(Color.BLACK);
        //achsen
        plot.getGraphWidget().getDomainOriginLabelPaint().setColor(Color.BLACK);
        plot.getGraphWidget().getDomainOriginLinePaint().setColor(Color.BLACK);
        plot.getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);


        plot.getDomainLabelWidget().position(0, XLayoutStyle.ABSOLUTE_FROM_CENTER, 0, YLayoutStyle.RELATIVE_TO_BOTTOM,  AnchorPosition.BOTTOM_MIDDLE);
        plot.setDomainLabel("Zeitpunkt der Messung");
        plot.setBorderStyle(Plot.BorderStyle.NONE, null, null);

        //Remove legend and title
        plot.getLayoutManager().remove(plot.getLegendWidget());
        plot.getLayoutManager().remove(plot.getTitleWidget());



        //Position und Größe
        plot.getGraphWidget().setSize(new SizeMetrics(
                50, SizeLayoutType.FILL,
                50, SizeLayoutType.FILL));
        plot.getGraphWidget().position(
                0, XLayoutStyle.RELATIVE_TO_RIGHT,
                0, YLayoutStyle.RELATIVE_TO_TOP,
                AnchorPosition.RIGHT_TOP);



        // reduce the number of range labels
        //plot.setTicksPerRangeLabel(3);
        plot.getGraphWidget().setDomainLabelOrientation(-45);





        // get rid of decimal points in our range labels:
        //plot.setRangeValueFormat(new DecimalFormat("0"));


        //plot.setMarkupEnabled(true);
        //plot.setPlotMargins();
        //plot.setPlotPadding(0, 0, 0, 0);
        plot.getGraphWidget().setPaddingBottom(200);
        plot.getGraphWidget().setDomainLabelVerticalOffset(50);


    }





    private void initUI() {
        myButton = (Button) myView.findViewById(R.id.ButtonDisplayData);

        myButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //plot.clear();
                //plot.redraw();
                plot.clear();
                getUserInput();
                readDataFromFile();
                displayData();
            }
        });

        radioGroup = (RadioGroup) myView.findViewById(R.id.radioGroup);
        numberOfDataPoints = (EditText) myView.findViewById(R.id.numberOfDataPoints);

        // initialize our XYPlot reference:
        plot = (XYPlot) myView.findViewById(R.id.mySimpleXYPlot);

        formatChart();


    }

    private void getUserInput()
    {
        String str = numberOfDataPoints.getText().toString();

        if(str.equals(""))
        {
            MAX_DATA = DEFAULT_MAX_DATA;
        }
        else
        {
            MAX_DATA = Integer.valueOf(str);
        }
    }

    private enum Sensors{
        TEMPERATURE,
        HUMIDITY,
        LUMINOSITY,
        RAIN,
        NONE
    }




    private void readDataFromFile()
    {
        datasetcount = getDatasetCount();

        Sensors checkedSensor = getCheckedSensor();

        switch(checkedSensor){
            case NONE:
                return;
            case TEMPERATURE:
                readFromFile("Temp");
                return;
            case HUMIDITY:
                readFromFile("Humidity");
                return;
            case LUMINOSITY:
                readFromFile("Luminosity");
                return;
            case RAIN:
                readFromFile("Rain");
                return;
            default:
                return;
        }

    }

    private Sensors getCheckedSensor()
    {
        int id = radioGroup.getCheckedRadioButtonId();
        if (id == -1){
            return Sensors.NONE;
        }
        else{
            if (id == R.id.radioButtonSensor1){
                return Sensors.TEMPERATURE;
            }
            if (id == R.id.radioButtonSensor2){
                return Sensors.HUMIDITY;
            }
            if (id == R.id.radioButtonSensor3){
                return Sensors.LUMINOSITY;
            }if (id == R.id.radioButtonSensor4){
                return Sensors.RAIN;
            }
        }
        return Sensors.NONE;
    }

    /**
     * liest die Wertepaare aus dem Logfile und speichert sie in Arrays zur Übergabe an das Chart
     *
     */
    private void readFromFile(String filename)
    {
        String[] parts;
        count = 0;
        xValues = new Long[MAX_DATA];
        yValues = new Double[MAX_DATA];

        //alle mit null initialisieren (dass nicht belegte Plätze vom Diagramm richtig ignoriert werden)
        for (int i = 0; i < MAX_DATA; i++)
        {
            xValues[i] = null;
            yValues[i] = null;
        }

        try {
            InputStream inputStream = getActivity().openFileInput(filename);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";

                //nur letzte maximal 200 bzw MAX_DATA werte verarbeiten
                for(int i = 0; i < (datasetcount - MAX_DATA); i++)
                {
                    bufferedReader.readLine();
                }

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    if(receiveString.contains(";"))
                    {
                        parts = receiveString.split(";"); //parts[0] = time in milliseconds since 1970, parts[1] = tempvalue
                        if(parts[0] != null && parts[1] != null && !parts[0].equals("null") && !parts[1].equals("null"))
                        {
                            xValues[count] = Long.valueOf(parts[0]);

                            if(parts[1].contains(","))
                            {
                                yValues[count] = Double.valueOf(parts[1].replace(",","."));
                            }
                            else
                            {
                                yValues[count] = Double.valueOf(parts[1]);
                            }
                            count ++;
                        }
                    }
                }

                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            //Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            //Log.e("login activity", "Can not read file: " + e.toString());
        }
    }


    private int getDatasetCount()
    {
        Context context = getActivity();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.dataset_counter_preference_file_key), Context.MODE_PRIVATE);

        return sharedPref.getInt(getString(R.string.dataset_counter), 0);
    }

}

