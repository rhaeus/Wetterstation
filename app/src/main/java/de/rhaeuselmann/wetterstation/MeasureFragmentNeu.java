package de.rhaeuselmann.wetterstation;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viewpagerindicator.TitlePageIndicator;

/**
 * Created by Ramona on 19.04.2015.
 */
public class MeasureFragmentNeu extends Fragment
{
    //Member Variables


    //startView Pager
    ViewPager mViewPager; //The {@link ViewPager} that will host the section contents.
    SectionsPagerAdapter mSectionsPagerAdapter;
    TitlePageIndicator indicator = null;

    BluetoothCommunication mChatService;



    //////////////////////////////////

    //Constructor
    public MeasureFragmentNeu(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setHasOptionsMenu(true);
        mSectionsPagerAdapter = new SectionsPagerAdapter(
                getFragmentManager(), getActivity());

        //TODO ceck was des is
        this.setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //viewpager start
        View rootView = inflater.inflate(R.layout.viewpager_meas, container, false);
//        mSectionsPagerAdapter = new SectionsPagerAdapter(
//                getFragmentManager(), getActivity());

        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        //Bind the title indicator to the adapter
        indicator = (TitlePageIndicator)rootView.findViewById(R.id.indicator);
        indicator.setViewPager(mViewPager);

        //Styling Tabs
        final float density = getResources().getDisplayMetrics().density;
        indicator.setBackgroundColor(getResources().getColor(R.color.actionbar_color)); //Color.parseColor("#303F9F"));
        //indicator.setFooterColor(0xFFAA2222);
        indicator.setFooterColor(0xFFFFFF);
        indicator.setFooterLineHeight(0 * density); //abstand zwischen indicator un fragment
        indicator.setFooterIndicatorHeight(5 * density); //3dp
        indicator.setFooterIndicatorStyle(TitlePageIndicator.IndicatorStyle.Underline);
        indicator.setTextColor(0xFFFFFFFF);
        indicator.setSelectedColor(0xFFFFFFFF);
        indicator.setSelectedBold(true);

        return rootView;
    }



}
